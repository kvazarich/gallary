var   express = require('express')
	, http = require("http")	  
	, app = express()
///config///
port = 3000;
var pathSeparator = (__dirname.indexOf("\\") != -1) ? "\\" : "/";
var dir = __dirname+pathSeparator+'static';
///config///
app.listen(port);
console.log("SITE: started on " + port);
app.get('*',function(req,res){
	res.sendFile(dir + '/index.html');
});